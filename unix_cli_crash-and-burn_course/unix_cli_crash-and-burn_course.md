
unix cli crash-and-burn course
==============================

this document is intended to provide a quick overview of everything a user
needs to know to get started working with a modern unix command line, with
focus given to those weird little bits that might seem natural to those of us
who do this every day but utterly foreign to a newcomer. it might be tough at
first, but stick with it and things will start to make sense, i promise!

a note to the experienced reader: this quick overview is geared towards
comprehensibility rather than always being "correct", and, as such, there are
some not-quite truths here and there. please try to ignore them (or,
alternatively, report them if it's something too glaring, i guess).

first, the basics
-----------------

### what is a "shell"?!

a shell is (for the purposes of this tutorial) a program that provides a
command line interface (cli) to a user. that is to say, it's the program that
reads commands you type, does things, and spits output back at you. when a user
logs into a unix-like system, the shell is the first process executed under
that user's account ("process" meaning "a currently-running instance of a
program"). this instance of the shell is called, predictably, a "login shell".
the user might also run a second instance of a shell later on (by e.g. opening
up a terminal emulator). that instance is a "non-login shell". 

you might think "but when i login, i don't get a shell. i get a desktop!!", or
something to that effect. modern unix-likes that run graphical desktops
automatically do so by using a "hook" of some kind so that the login shell
immediately spawns a graphical environment. if your system is linux+X11, you
should be able to press control+alt + a function key to switch to a different
linux terminal, where you can use a login shell directly.

anyways, there are lots of different shells (with names like sh, bash, zsh,
tcsh, csh, ksh, fish and so on), but they mostly work pretty similarly, so that
if you pick up one you'll easily be able to adapt to another if necessary. i
can't discuss all the nuances of these different options in one place, though,
or things would get too confusing, so the rest of this document will default to
using bash-like syntax for things, bash being the default shell on most linux
distributions.

because there are so many different shells, and so many different ways to
configure them, the "prompt" (the bit of text that the shell prints out to tell
you "you can write things, now! i'm listening!") can vary a lot from system
to system. mine looks like this:

	┌[shmibs@kino ~]
	└: you write things here!

but yours might be something more like:

	[shmibs@kino ~]$ writing goes here this time! usually used for bash

or

	shmibs@shmibbles:~ % this one's used by tcsh on my server

or even just

	$ this one's kind of plain〜

### what is a "command"?!

ok, so i've mentioned that the shell provides a "command line interface": that
is, a "line" into which one inputs "commands" in order to "interface" with the
computer. what then is a "command"?

it's a bit of a nebulous term, but, for the scope of this document, i'll define
it as "a message sent from the user to the shell which prompts some sort of
action". generally, in a shell without weird fancy keybindings or whatever,
this means "typing in a string of characters and pressing enter".

since that definition is pretty broad (and thus kind of useless in itself),
i'll further specify what sorts of pieces make up a command in most shells:

1. command name / specifier (this is generally referred to as "command". hence
   my saying the term is a bit nebulous)
2. strings
3. file paths
4. options
5. variables
6. substitutions
7. input / output redirectors
8. expansions
9. and more!!

### what is a "command name" / "command specifier"?!

as i mentioned above, this is generally just referred to as "the command". i've
chosen to give it a different name here, though, because it really is a
separate thing. from here on, for the sake of brevity, i'll just refer to it as
command name.

so, most commands will contain a command name as the first word in the command
(word as in whitespace-free string of characters). for example, in the command
`cd /usr/share/`, the command name is "cd".

the command name specifies what sort of function the command will perform. or,
more accurately, it specifies the name of something that implements
functionality, where that something is the name of a program, the name of a
function built into the shell, or the name of a user-defined function or alias.
i'll explain how command names are associated with these bundles of
functionality later, but for now let's move on.

### what is a "string"?!

a string is a sequence of characters, separated from its surroundings by
whitespace. in the command `service sshd status`, "sshd" and "status" are both
strings.

since it is sometimes useful to include whitespace within a string,
quotation marks can also be used, with any characters between them considered
part of the string. in `echo "hullo there"`, "hello there" is a single
string. with the quotation marks removed, `echo hullo there`, there are two
strings, "hullo" and "there".

to prove this to yourself, you can try a simple test. add a few extra spaces
between hullo and there in the first command and the printed result will also
contain those extra spaces. add extra spaces in the second command, however,
and they will be ignored, as only the two strings will be printed with a space
separating them.

another important tool for writing strings is escaping. like you may have seen
elsewhere if you've done any programming, bash uses the forward slash character
to escape other characters. that is to say, if a forward slash is encountered
somewhere outside of an insertion or quoted string, the following character is
used directly in the command rather than first being interpreted by the shell.
thus, in `echo hullo\ \ \ there`, a single string, "hullo   there" is used in
the command, and the printed result will contain those three spaces.

**note:** strings can also use single quotes, and there is a difference between
the two. double-quoted strings allow the shell to interpret substitutions and
include them in the string at that point, while single-quoted strings indicate
that all characters should be used directly and no substitutions performed.

### what is a "file path"?!

ok, i lied a little. "file paths" and "options" are really just strings. the
shell itself has no knowledge of what strings are "file paths" and what are
"options" (well, a modern shell might, but there is no difference,
traditionally). what strings are interpreted as file paths, options, or
whatever else, then, is completely up to what functionality the command name
refers to.

still, it is important to consider these two special cases individually, as
there are conventions observed by (nearly) everyone.

file paths can be written in in one of two forms: relative and absolute. an
absolute file path always begins with a backslash, which refers to the root of
your system's file tree. unlike disk operating systems, every single file in a
unix system can be accessed somewhere as a leaf of this tree (**note:** i mean
"tree" in the data structure sense. see
[here](https://en.wikipedia.org/wiki/Tree_(data_structure)) for more details).
from this root directory, add a list of the subdirectories you wish to enter,
each separated by backslashes, until reaching the desired file, like so:
`/usr/share/git/README`.

the other way to reference a file's location is with a relative path, which
looks something like this: `images/grr/STINKY\ DOG\ BUTT.png`. note the lack of
a backslash at the beginning of the path. this indicates the files location is
relative to the "current working directory" rather than the filesystem root.
the shell has, at all times, a "current working directory", and all commands
which perform filesystem actions will generally default to acting on files
within this directory. when the shell is first run, the current directory is
usually set to the users home directory (traditionally located at
`/home/<username>/`). when a relative file path is used, it indicates basically
that the current directory should be used as the filesystem root, so, when in a
user's home directory, `documents/` is eqivalent to
`/home/<username>/documents`.

it's not just the shell, in fact, that has a current working directory. on a
unix system, every process (remember, a process is a program currently
executing) has a working directory, meaning these two methods of specifying
file paths should be universally available to programmers as well.

**note:** this all may seem pretty straight-forward, but there are a few
potentially tricky things to keep in mind. file names are _case sensitive_
(`name.txt` is not the same as `Name.txt`) and, these days, are mostly [utf-8
encoded](https://en.wikipedia.org/wiki/UTF-8), meaning they can contain any
arbitrary character other than a backslash, to ensure unambigious file paths,
and the [null character](https://en.wikipedia.org/wiki/Null_character). this
means file names can contain spaces, for which you need either add escapes or
wrap the path in quotes. this also means the following should all be valid file
names on your system: `deštník.pdf`, `傘.png`, `☂.gif`, or even
`UMBRELLA\nELLA\nEY!.ogg` (where `\n` represents the
[newline](https://en.wikipedia.org/wiki/Newline) character). this last case is
important to remember, as many core unix utilities default to operating on text
line by line, meaning a few shell commands that seem to work might actually
fail if passed one of these files containing newlines, treating it as multiple
files.

and one last but very important thing: along with the actual contents, every
directory in a unix filesystem contains two special directories named `.` and
`..`. `..` is an [alias](https://en.wikipedia.org/wiki/Aliasing_(computing)) of
(another name for) that directory's "parent directory", or the directory which
contains that directory. `.` is alias of the directory itself. the file path
`/usr/share/git/README`, then, could be expressed equivalently, though
redundantly, with `/usr/../usr/share/git/./README`. although these special
directories are a bit useless in absolute filepaths, they can be very useful
for relative filepaths. `..` allows file paths to display locations higher up
in the filesystem tree as well as further down, meaning relative file paths
which start somewhere already part-way down the file tree can refer to things
further up. although it may not seem like it `.` is also useful, primarily for
two reasons: firstly, it allows a command to act on the current directory
without specifying its name. the copy command, then, can be used like `cp <some
file> .` to copy a file into the current directory, rather than using the more
verbose `cp <some file> ../<current directory>`. the second important use is
for running programs contained in the current directory. the normal way to
refer to such a program would intuitively be `<program name>`. however, the
shell reserves such single-word command names for installed programs. otherwise
a user might create a file in the current directory named "cp", for example,
and lose the ability run the `cp` command, as the shell would try to run the
local file instead. to specify that you do want to execute that local file,
then, you can use `.` like so: `./cp`.

### what is an "option"?!

this section is a bit trickier than the last. "options" are one-time settings
for the behaviour of a command, giving them in-line information to change their
behaviour. running the command `ls` should give a list of the files in the
current directory, like so:

	┌[shmibs@lain web/tutorials]
	└: ls
	README.md  unix_cli_crash-and-burn_course

if you add the `-l` option, though, the behaviour will change, displaying the
file list using the "long listing format":

	┌[shmibs@lain web/tutorials]
	└: ls -l
	total 8
	-rw-r--r-- 1 shmibs users   12 Feb 18 16:14 README.md
	drwxr-xr-x 2 shmibs users 4096 Feb 20 11:57 unix_cli_crash-and-burn_course

that seems simple enough, right? the tricky part, though, is that the format
for specifying options is not universal.

the "normal" way for specifying an option is to precede it with a dash, like
i've done above. so, in `ls -l`, the option is `l`, and the `-` indicates that
`l` is an option. some options also take arguments.
[ffmpeg](https://ffmpeg.org/) (which, by the way is really really cool and can
do just about anything video-related) can convert the first 4 seconds of an mp4
into a webm like so: `ffmpeg -i cats.mp4 -t 4 four\ seconds\ of\ cats.webm`. in
this example, `-i` takes, as an argument, the input file ffmpeg should convert
from, and `-t` takes the time length of the output file in seconds. many
commands also take options which are one or more words in length, or "long
options", and these are usually preceded with two dashes rather than one, like:
`grep --regexp '^2.*' words.txt`, which tells
[grep](https://en.wikipedia.org/wiki/Grep) to search in words.txt, using a
[regular expression](https://en.wikipedia.org/wiki/Regular_expression) which
finds every line beginning with a '2'. **note:** long options can typically
have arguments specified using an `=` as well (so `grep --regexp='^2.*'
words.txt` is equivalent to the command above).

if this were all there was to it, things would be simple enough. like i said
above, though, the way options are written can differ from command to command.
sometimes single-letter options that don't take arguments can be grouped
together using a single dash. `ls -la`, for example, lists files using both the
`-l` output format mentioned above and the `-a`, or "all", option, which
includes [hidden
files](https://en.wikipedia.org/wiki/Hidden_file_and_hidden_directory).  other
commands might require that these single-letter options remain separate,
though. the convention of using `--` as a prefix for long options is not
universal either. the [find](https://en.wikipedia.org/wiki/Find) command, for
example, uses a single `-`, like so: `find . -name cat.png` (which searches
[recursively](https://en.wikipedia.org/wiki/Recursion_(computer_science)) [in
the current directory and everything below it in the file tree] for files named
cat.png').

if this weren't confusing enough, some commands forgo using dashes altogether,
like `service` and `systemctl`, interface commands to different
[init](https://en.wikipedia.org/wiki/Init) systems (the programs that start all
the other programs when a computer is turned on). to check if an
[ssh](https://en.wikipedia.org/wiki/Secure_Shell)
[daemon](https://en.wikipedia.org/wiki/Daemon_(computing)) is running on the
computer, one would use `service sshd status` or `systemctl status sshd`. note
the lack of dashes anywhere AND the annoyingly inconsistent ordering (which is
one of those things that trips up even experienced users).

to make things even more confusing, even standard commands, guaranteed by
[POSIX](https://en.wikipedia.org/wiki/POSIX) to be available pretty much
everywhere, can have inconsistencies in their arguments. the
[freebsd](https://en.wikipedia.org/wiki/FreeBSD) version of `ls`, for example,
has an option `-w`, which "forces raw printing of non-printable characters".
this same `-w` option is used in the [GNU](https://en.wikipedia.org/wiki/GNU)
version of `ls` to specify the width of the printed output, which it takes as
an argument.

now, before you start to despair over all this back-and-forth weirdness, let me
introduce you to a command that will save you a lot of headaches: `man` (short
for "manual"). if your system has a program installed, chances are it has one
or more associated "man pages", which you can access by running `man <command
name>`. this will bring up a page of information showing what options can be
used in the command, an overview of the command's functionality, descriptions
of all the available options, and even usage examples and other useful
information. and man pages are not provided only for commands. there are
different "sections", labelled 1, 2, 3 etc, which man pages are classified
under, each pertaining to different things that need documenting, like library
calls, system calls, file formats, and even best practices. for more
information, you can run `man man` (and feel a little silly while doing so :P).

### what is a "variable"?!

ok, so up until this point we've been talking only about single commands run in
isolation. what a unix shell provides, however, is an entire
[interpreted](https://en.wikipedia.org/wiki/Interpreted_language),
[imperative](https://en.wikipedia.org/wiki/Imperative_programming) programming
language. this means that state can be stored and passed from one program to
the next in multiple ways. the way you're most likely to be familiar with
already, if you have any programming experience, is through
[variables](https://en.wikipedia.org/wiki/Variable_(computer_science)).

a variable, within the context of the shell, is a name to which can be assigned
a string value (...or an array, or, in modern shells, an integer, but don't
worry about those for now) and from which that value can be retrieved later.
so, to understand how to use variables, we must understand these two steps.

imperative languages are composed of a series of "statements", with each
statement executed one after the other, so that a variable might be set in one
statement and its value retrieved during the next. in this way, successive
statements can influence one another. we've seen one type of statement already:
the command statement. the other we'll discuss here is the assign statement,
which is used for assigning values to variables. they look something like this:
`varname="string value"`, i.e. the name of the variable, an `=`, and the value
you want the variable to hold. **note:** the lack of spaces surrounding the `=`
is necessary. in many programming languages, users are encouraged to insert
space surrounding an `=` for better readability. this is not possible in a
shell, however, as the variable name would then instead be recognised as a
command name, with `=` being an argument. **note:** assignment syntax differs
for tcsh, and the c shell it's based on, taking instead the form `set varname =
"string value"`. fish uses a similar notation, but drops the `=`. from here on,
i'll continue using bash-esque syntax, but it's important to be aware that the
syntax is not universal.

now that we've assigned a value to "varname", it can be retrieved in a command
later on using a "variable substition", which looks something like this: `echo
$varname`. the `echo` command takes string arguments and prints them. the
`$varname` syntax tells the shell to look up the current value of `varname` and
replace it inline. 

**note:** so then running

	[shmibs@kino ~]$ varname="string value"
	[shmibs@kino ~]$ echo $varname
	string value

is equivalent to running

	[shmibs@kino ~]$ echo "string value"
	string value

right? probably not, actually. remember, wrapping an element in quotes
indicates everything between them is a single string. however, the quotation
marks themselves are not part of the string. try running the first snippet in
bash, then, and the shell inserts the literal character string "string value",
without quotation marks, into the second command. so what it's _actually_
equivalent to is:

	[shmibs@kino ~]$ echo string value
	string value

which looks like the same thing, right? the difference, though, is that
"string" and "value" are then interpreted as being two separate arguments of
the command, rather than just one. to prove it to yourself, you can try running
something like:

	[shmibs@kino ~]$ varname="string          value"
	[shmibs@kino ~]$ echo $varname
	string value

the extra whitespace in the string is interpreted as space between two
arguments and stripped away, after which "string" is printed and then "value".
if you want them to be interpreted as a single string instead, you'll have to
wrap the insertion in quotation marks as well:

	[shmibs@kino ~]$ varname="string          value"
	[shmibs@kino ~]$ echo "$varname"
	string          value

this simple mistake has led to decades worth of sometimes-catastrophic bugs and
unintended behaviour. if you aren't required to use a shell that implements
this dangerous behaviour, it's best to avoid it entirely. i would recommend
[zsh](http://www.zsh.org/), which automatically treats variable insertions as a
single string with or without wrapping quotation marks, reserving
multiple-string substitution to the domain of arrays. it avoids several other
unintuitive, common-points-of-failure in shell-scripting this way as well,
while still keeping close to bash-esque syntax for easy transitions between the
two. if you prefer things that are cute and trendy, though,
[fish](https://fishshell.com/) might be a better choice ^_^.

before moving on, one last thing: some variables are standardised bits of a
modern unix system, often defined automatically and used by scripts and
programs to better adapt to their environment. these are called, fittingly,
"environment variables", and they're usually capitalised, like this:

	┌[shmibs@lain ~]
	└: echo $TERM 
	rxvt-unicode-256color

environment variables can also be set for a single command to override their
default values:

	┌[shmibs@lain ~]
	└: LANG=ja_JP.UTF-8 date +$'%a, %b %d, %H:%M:%S'
	金,  2月 24, 23:46:08

this will not override the value of the variable when it's passed as an
argument, however, only the value as accessed within the program run by the
command:

	┌[shmibs@kino ~]
	└: LANG=ja_JP.UTF-8 echo $LANG
	en_GB.UTF-8

a few often-available environment variables to be aware of are `$EDITOR`,
`$HOME`, `$LANG`, `$PATH`, `$PWD`, `$TERM`, `$SHELL`, and `$USER`. i'll leave
researching their possible uses as an excercise for the reader.

### what are "substitutions"?!

"substitutions" are character sequences which a shell catches and in-line
replaces for the values they describe. there are four kinds that i will discuss
here:

* variable substitutions
* subshell substitutions
* arithmetic substitutions
* process substitions

**variable substitutions** we've already touched on briefly above. every time
we used a variable, it was via a substitution, with the shell recognisning a
sequence of characters referring to a variable, like `$TERM`, and inserting it
in place before the command is executed.

the simplest variable insertions are simply the name, prefaced with a `$`. what
happens, though, if you want to insert a variable directly into a string, like
the following?

	┌[shmibs@kino ~]
	└: pref=super

	┌[shmibs@kino ~]
	└: echo "$prefcilious: the state of being altogether far too silly"
	: the state of being altogether far too silly

this looks for a variable named "prefcilious", which does not exist, and so
inserts nothing. to get the expected result, the variable name can, in most
shells, be surrounded with curly braces:

	┌[shmibs@kino ~]
	└: echo "${pref}cilious: the state of having excess hair"
	supercilious: the state of having excess hair

some shells have advanced syntax for variable insertions which builds upon this
braced syntax. in zsh, for example, one can print the uppercase conversion of a
variable like so:

	┌[shmibs@kino ~]
	└: echo ${pref:u}
	SUPER

or, if a variable contains a file path, print just the containing directory,
file name, or file extension:

	┌[shmibs@kino ~]
	└: filename=/path/to/cool_picture.jpg

	┌[shmibs@kino ~]
	└: echo ${filename:h}
	/path/to

	┌[shmibs@kino ~]
	└: echo ${filename:t}
	cool_picture.jpg

	┌[shmibs@kino ~]
	└: echo ${filename:e}
	jpg

or even perform in-line substitutions, for those times you don't want to break
out sed:

	┌[shmibs@kino ~]
	└: echo ${filename/cool/lame}
	/path/to/lame_picture.jpg

...but that's enough zsh-advertising :P. on to the next thing!

**subshell substitutions** are substitutions which run commands and in-place
return their outputs. traditionally, they're surrounded with `\`` characters
(called backticks), like this:

	┌[shmibs@kino ~]
	└: echo `pwd`
	/home/shmibs

but this syntax has been supplanted in most shells with `$(` and `)`, like so:

	┌[shmibs@kino ~]
	└: echo $(pwd)
	/home/shmibs

which is easier to read, better matches other substitution syntax, and, more
importantly, can be nested, like so:

	┌[shmibs@kino ~]
	└: echo $(echo $(pwd))
	/home/shmibs

 an important thing to remember is that, like the name would suggest, these
 commands are run in a new shell, called a child, created for this command and
 dying afterwards. this means that they cannot affect variables etc. defined in
 the parent shell.

 the subshell *inherits* variables from the parent, so a command like this is
 valid:

	┌[shmibs@kino ~]
	└: var=boop

	┌[shmibs@kino ~]
	└: echo $(echo $var)
	boop

however, `var` in the subshell is a copy of `var` in the parent shell, so
modifying it will not modify the parent's version of `var`:

	┌[shmibs@kino ~]
	└: echo $(var=doop; echo $var)
	doop

	┌[shmibs@kino ~]
	└: echo $var
	boop

as an aside, this introduces something important we've yet to touch on: `;`.
basically, a `;` allows you to write multiple statements inline, rather than
adding a new line for each. so

	┌[shmibs@kino ~]
	└: var=doop; echo $var
	doop

is equivalent to

	┌[shmibs@kino ~]
	└: var=doop

	┌[shmibs@kino ~]
	└: echo $var
	doop

another thing worth mentioning, though not something you're likely to use as
often, is that a subshell can be run without capturing its output as insert by
using only the parentheses, without a `$`, like so:

	┌[shmibs@kino ~]
	└: (echo beep)
	beep

**arithmetic substitutions**, in bash-alikes, are very similar in appearance to
subshell substitutions, using double parentheses rather than single:

	┌[shmibs@kino ~]
	└: echo $((1+2))
	3

...and that's about all there is to say about them, really. if you know c-like
arithmetic expressions, you should feel right at home.

	┌[shmibs@kino ~]
	└: num=13; echo $(( 3 + ($num % 5) )) 
	6

and finally....

**process substitions** are useful when you want to "pipe information through"
a file which performs named file I/O rather than reading from and writing to
stdin and stdout. you'll understand the significance of this after reading the
next section, about "input and output redirection".

process substitutions come in input and output varieties, wrapped respectively
with `<()` and `>()`, and resolve to the name of an anonymous file somewhere,
which the shell will handle for you. for input redirection, the output of
whatever is run between the parentheses will be written into this anonymous
file, from which it can then be read, and the reverse is true for output
redirection. i'll go into this more in the next section, where it will
hopefully make a bit more sense.

### what are "input and output redirectors"?!

so running programs can read from and write to files, right? and, if you're
still reading this, you're probably familiar with the "standard" input and
output files associated with a program, accessible through things like C's
`getc` and `printf` functions. when running a command in-terminal, these
standard files (abbreviated `stdin` and `stdout`) will have their contents
directly read from whatever input is typed into the terminal and have
whatever's written to them printed to the terminal respectively.

input and output directors, then, are modifiers which act on this standard IO.
they use the following symbols: `<`, `>`, `<<`, `>>`, `|`



### what are "expansions"?!

moving forwards: advanced concepts
----------------------------------

### arrays

### control flow

### functions

some practical examples
-----------------------

ok, now that we're through all that, time to give some examples of things you
can start applying right away! (or at least get an idea of how this stuff
day-to-day works)

### mass-rename files in a directory

### make your aliases portable

### fizzbuzz (i.e. asynchronous message parsing)

weird unix-y concepts
---------------------

the following are a few concepts which are taken for granted as goals or
realities in a modern flavour of unix but may seem strange to an "outsider"

### "do one thing well"

generally referred to as "the unix philosophy", the concept that a program or
utility should tackle one well-defined problem and solve it "elegantly", rather
than half a dozen problems poorly, has long been a central thought for any
programmer working within a unix environment. recently, however, there has been
some internal conflict over this concept, as the rise of massive, integrated
systems like GNOME and SystemD has begun to conquer the modern unix workstation.

currently confined mostly to linux distributions, this development still is
very worrying both for reasons of stability and diversity. such a "monolithic"
system may be nice when it works, but when it breaks it's very difficult to
fix, and, because of it's tightly-coupled nature, breakage in one place often
means breakage in many places. additionally, the assumptions such a system make
about a user's system mean that many once-possible configurations and
modifications become impossible.

despite these few "intrusions", however, the majority of the unix landscape
remains shaped by this concept of "play nicely" modularity, with the origin of
it all, the unix command line, in many ways preserved since its very first
iteration.

### "everything is a file"

likely the most noticeable manifestation of this concept to a user coming from
a windows environment will be the lack of "disks" (i.e. `C:\\`, `D:\\`,
`LMNOP:\\`, `ETC:\\`). instead, physical storage is represented by one or more
<emph>mountpoint</emph>s somewhere within the system's file tree we discussed
above. a _mountpoint_ is just a directory that points to the contents of that
drive, and a directory is itself just a "file that holds files". the first
benefit of this approach is an increased uniformity: any tool which can operate
on a directory can do the same on a mountpoint with no code modifications. the
second is flexibility: a mountpoint might point to different backing
filesystems at different times, or to a filesystem created from multiple
underlying disks (such as a RAID), or even to an entirely virtual filesystem
(something like FUSE, which allows a programmer to write an interface to FTP,
SSH, or anything at all and present it to the user as a standard mountpoint).

the "everything is a file" concept is much more pervasive than just that,
however. on a modern linux system, for example, command binaries, program
configurations, newly attached devices, asynchronous locks, memory shares,
inputs and outputs for communicating with the kernel or its modules, and even
random number generators all appear as standard files, part of the same file
tree and accessible through the same file read and write commands as any other.
this enormous degree of flexibility (and the unified system of access control
that is its side-effect) take away many of the headaches of managing the ever
shifting modular environments of modern computing, with devices and services
appearing and disappearing regularly during runtime.

### package repositories

though perhaps not readily available to proprietary flavours of unix (MacOS et
al.), <emph>package repo</emph>s have become central tenets of the modern open
unix environment (BSDs, linux distros, etc). a package repo provides a
centralised interface to software "packages", a bundle of all the bits and
pieces necessary to run a particular program or something similar. unlike
systems which install programs through website downloads or offline media these
packages are generally guarded from third-party tampering through hashing and
being signed by trusted key-holders who put them together. the processes of
downloading, unpacking, and installing are carried out by a single program
called a _package manager_. this package manager then registers every file it
installs as being owned by that package so that it can cleanly reverse the
process at any time later on, uninstalling the package without permanent damage
or "left-over cruft". a package manager also tracks _dependencies_, meaning it
knows and can pre-install other packages which are required for a given package
to function, or later remove them when the package that needed them is also
removed. these combined traits, when properly applied, allow for a regularly
updated machine to remain clean and secure and cool things like that.

a list of common commands that You Should Know
----------------------------------------------

remember, `man` is your friend!

### navigation
<dl>
	<dt>ls</dt>
	<dd>list files or the contents of folders</dd>
	<dt>cd</dt>
	<dd>change the current working directory</dd>
	<dt>find</dt>
	<dd>search for and list files that match given criteria</dd>
	<dt>stat</dt>
	<dd>list information about a file</dd>
	<dt>file</dt>
	<dd>also list information about a file. more human-readable</dd>
	<dt>du</dt>
	<dd>display the sizes of files</dd>
	<dt>du</dt>
	<dd>display disc usage</dd>
	<dt>pwd</dt>
	<dd>"print working directory". more useful from scripts than interactive shell-ing, really</dd>
</dl>

### file manipulation
<dl>
	<dt>touch</dt>
	<dd>attempt to access files, creating them if they do not exist</dd>
	<dt>rm</dt>
	<dd>permanently remove files</dd>
	<dt>cp</dt>
	<dd>copy files from one location to another</dd>
	<dt>mv</dt>
	<dd>move files from one location to another</dd>
	<dt>rename</dt>
	<dd>rename files; mostly useful for bulk renaming</dd>
	<dt>mkdir</dt>
	<dd>create directories</dd>
	<dt>rmdir</dt>
	<dd>permanently remove directories</dd>
	<dt>chmod</dt>
	<dd>modify a file's <a href="https://en.wikipedia.org/wiki/File_system_permissions">permissions</a></dd>
	<dt>chown</dt>
	<dd>give ownership of a file to a different user. useful for tweaking permissions of things</dd>
	<dt>mount</dt>
	<dd>join an external filesystem to the system file tree (i.e. make it available through a directory somewhere)</dd>
	<dt>umount</dt>
	<dd>remove a previously joined external filesystem from the system tree</dd>
	<dt>ln</dt>
	<dd>create links between files, either hard or symbolic</dd>
	<dt>dd</dt>
	<dd>directly copy the contents of a file or device to another</dd>
</dl>

### text manipulation
<dl>
	<dt>echo</dt>
	<dd>print arguments to stdout</dd>
	<dt>printf</dt>
	<dd>print arguments to stdout, with advanced formatting</dd>
	<dt>cat</dt>
	<dd>print the contents of files to stdout</dd>
	<dt>less / more</dt>
	<dd>read stdin and "page" it, allowing the user to review</dd>
	<dt>grep</dt>
	<dd>search for patterns within files</dd>
	<dt>sed</dt>
	<dd>perform inline substitutions in files, among other things. see <a href="http://www.grymoire.com/Unix/Sed.html">here</a></dd>
	<dt>diff</dt>
	<dd>calculate the difference between two files</dd>
	<dt>tr</dt>
	<dd>translate or delete characters in a pipeline</dd>
	<dt>wc</dt>
	<dd>count the number of characters / words / lines etc in files</dd>
	<dt>cut</dt>
	<dd>chop out delimited chunks of lines. i prefer using sed for this sort of thing, though.</dd>
</dl>

### program creation
<dl>
	<dt>read</dt>
	<dd>store input from stdin into shell variables</dd>
	<dt>getopts</dt>
	<dd>provide powerful, easy-to-use option parsing for shell programs</dd>
</dl>

### other
<dl>
	<dt>ssh</dt>
	<dd>provide encrypted access to a shell on a server</dd>
	<dt>tar</dt>
	<dd>create "tape archives" from files and folders, kinda like the ubiquitous "zip file"</dd>
	<dt>gzip</dt>
	<dd>along with gunzip, provide in-place compression of files</dd>
	<dt>awk</dt>
	<dd>a powerful language in its own right that's too complicated to get into here</dd>
</dl>

### personal favourites (less "standard")
<dl>
	<dt>vim</dt>
	<dd>best text editor that ever did was</dd>
	<dt>ranger</dt>
	<dd>removes reliance on coreutils for file management, and is generally just really useful</dd>
	<dt>atool</dt>
	<dd>a suite of scripts which can handle (compress|decompress|preview|modify)-ing just about any kind of archive</dd>
	<dt>ffmpeg</dt>
	<dd>a do-it-all tool for transcoding and streaming audio and video files, with some fancy filters and effects built-in</dd>
</dl>
